import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import {CardsComponent} from './components/cards/cards.component';
import {ExpandableCardsComponent} from './components/expandable-cards/expandable-cards.component';
import {ModalsComponent} from './components/modals/modals.component';

export const routes: Routes = [
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Home',
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'components'
      }
    ]
  },
  {
    path: 'components',
    component: FullLayoutComponent,
    data: {
      title: 'Components',
      show: true
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'cards'
      },
      {
        path: 'cards',
        component: CardsComponent,
        data: {
          title: 'Cards',
          show: true
        }
      },
      {
        path: 'expandable-cards',
        component: ExpandableCardsComponent,
        data: {
          title: 'Expandable Cards',
          show: true
        }
      },
      {
        path: 'modals',
        component: ModalsComponent,
        data: {
          title: 'Modals',
          show: true
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
        paramsInheritanceStrategy: 'always'
      })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
