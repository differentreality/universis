import { Component } from '@angular/core';

@Component({
    selector: 'app-preview',
    template: `<router-outlet></router-outlet>`,
})
export class AppComponent  { name = 'Angular'; }
