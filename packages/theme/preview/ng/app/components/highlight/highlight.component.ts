import {Component, ElementRef, Input, OnInit} from '@angular/core';
import hljs from 'highlight.js/lib/highlight';
import prettier from 'prettier/standalone';
import htmlParser from 'prettier/parser-html';
import angularParser from 'prettier/parser-angular';
import xmlLanguage from 'highlight.js/lib/languages/xml';
import javascriptLanguage from 'highlight.js/lib/languages/javascript';
import typescriptLanguage from 'highlight.js/lib/languages/typescript';
import jQuery from 'jquery';
// register languages
hljs.registerLanguage('xml', xmlLanguage);
hljs.registerLanguage('javascript', javascriptLanguage);
hljs.registerLanguage('typescript', typescriptLanguage);


function escapeHTML(unsafe) {
  if (typeof unsafe == null) {
    return;
  }
  return unsafe
      .replace(/&/g, '&amp;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
      .replace(/"/g, '&quot;')
      .replace(/'/g, '&#039;');
}

@Component({
  selector: 'app-highlight',
  template: `<div class="card">
                      <div class="card-body">
                        <pre><code class="hljs"></code></pre>
                      </div>
                    </div>
        `,
  styles: []
})
export class HighlightComponent implements OnInit {

  @Input() highlight: any;

  constructor(private element: ElementRef) { }

  ngOnInit() {
    if (this.highlight) {
      const targetElement = jQuery(this.highlight);
      if (targetElement) {
        const codeElement = jQuery(this.element.nativeElement).find('code').get(0);
        // format html
        const html = escapeHTML(prettier.format(targetElement.html(), {
          parser: 'html',
          printWidth: 140,
          htmlWhitespaceSensitivity: 'ignore',
          useTabs: false,
          plugins: { parsers:  htmlParser }
        }));
        if (codeElement) {
          codeElement.innerHTML = html;
          hljs.highlightBlock(codeElement);
        }
      }
    }
  }

}
