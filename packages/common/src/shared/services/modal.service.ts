import {ApplicationRef, ComponentFactoryResolver, EmbeddedViewRef, Injectable, Injector, EventEmitter} from '@angular/core';
import {DialogComponent, DIALOG_BUTTONS} from '../components/modal/dialog.component';
import { ToastService } from './toast.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


/**
 *
 * Displays a Modal window or a type of Notification (based on choice the color changes)
 * @export
 * @class ModalService
 */
@Injectable()
export class ModalService {
  modalRef: any;
  config = {
    ignoreBackdropClick: true,
    keyboard: false,
    initialState: null,
    class: 'modal-content-base'
  };

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private appRef: ApplicationRef,
              private injector: Injector,
              private modalService: BsModalService) { }

  showDialog(title: string, message: string, buttons: DIALOG_BUTTONS = DIALOG_BUTTONS.Ok) {
    const componentRef = this.componentFactoryResolver
        .resolveComponentFactory(DialogComponent)
        .create(this.injector);
    componentRef.instance.title = title;
    componentRef.instance.message = message;
    componentRef.instance.buttons = buttons;
    // attach component to the appRef so that it's inside the ng component tree
    this.appRef.attachView(componentRef.hostView);
    // get DOM element from component
    const modalElement = (componentRef.hostView as EmbeddedViewRef<any>)
        .rootNodes[0] as HTMLElement;
    // append DOM element to the body
    document.body.appendChild(modalElement);
    return componentRef.instance.ngOnInit().then(() => {
      // show dialog
      return componentRef.instance.show().then( result => {
        // detach view
        this.appRef.detachView(componentRef.hostView);
        // destroy component ref
        componentRef.destroy();
        //
        return Promise.resolve(result);
      });
    });
  }

  openModal(template: any, customClass?: string): BsModalRef {
    let config;
    if (customClass) {
      config = JSON.parse(JSON.stringify(this.config));
      config.class = customClass;
    } else {
      config = this.config;
    }
    return this.modalRef = this.modalService.show(template, config);
  }
}
