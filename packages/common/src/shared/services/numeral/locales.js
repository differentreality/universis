(function (global, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['numeral'], factory);
    } else if (typeof module === 'object' && module.exports) {
        factory(require('numeral'));
    } else {
        factory(global.numeral);
    }
}(this, function (numeral) {

    (function() {
        numeral.register('locale', 'el', {
            delimiters: {
                thousands: '.',
                decimal: ','
            },
            abbreviations: {
                thousand: 'χιλ',
                million: 'εκ',
                billion: 'δισ',
                trillion: 'τρισ'
            },
            ordinal: function (number) {
                return 'ο';
            },
            currency: {
                symbol: '€'
            }
        });
    })();

    (function() {
        numeral.register('locale', 'el-gr', {
            delimiters: {
                thousands: '.',
                decimal: ','
            },
            abbreviations: {
                thousand: 'χιλ',
                million: 'εκ',
                billion: 'δισ',
                trillion: 'τρισ'
            },
            ordinal: function (number) {
                return 'ο';
            },
            currency: {
                symbol: '€'
            }
        });
    })();

    (function() {
        numeral.register('locale', 'cy', {
            delimiters: {
                thousands: '.',
                decimal: ','
            },
            abbreviations: {
                thousand: 'χιλ',
                million: 'εκ',
                billion: 'δισ',
                trillion: 'τρισ'
            },
            ordinal: function (number) {
                return 'ο';
            },
            currency: {
                symbol: '€'
            }
        });
    })();

    (function() {
        numeral.register('locale', 'cy-cy', {
            delimiters: {
                thousands: '.',
                decimal: ','
            },
            abbreviations: {
                thousand: 'χιλ',
                million: 'εκ',
                billion: 'δισ',
                trillion: 'τρισ'
            },
            ordinal: function (number) {
                return 'ο';
            },
            currency: {
                symbol: '€'
            }
        });
    })();



}));
