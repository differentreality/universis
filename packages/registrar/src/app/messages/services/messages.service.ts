import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  constructor(private _context: AngularDataContext,
              private _http: HttpClient) { }

  sendMessageToStudent(studentId, responseModel, actionId=null) {

    const formData: FormData = new FormData();
    if (responseModel.attachment) {
      formData.append('attachment', responseModel.attachment, responseModel.attachment.name);
    }
    if (responseModel.subject) {
      formData.append('subject', responseModel.subject);
    }
    if (actionId) {
      formData.append('action', actionId);
    }
    formData.append('body', responseModel.body);

    // get context service headers
    const serviceHeaders = this._context.getService().getHeaders();
    const postUrl = this._context.getService().resolve(`students/${studentId}/messages/send`);

    return this._http.post(postUrl, formData, {
      headers: serviceHeaders
    });
  }


}
