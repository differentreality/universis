import { Component, OnInit, ViewChild } from '@angular/core';
import * as EXAMS_LIST_CONFIG from './exams-table.config.json';
import { AdvancedTableComponent } from '../../../tables/components/advanced-table/advanced-table.component.js';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-exams-table',
  templateUrl: './exams-table.component.html',
})
export class ExamsTableComponent implements OnInit {

  public readonly config = EXAMS_LIST_CONFIG;

  @ViewChild('table') table: AdvancedTableComponent;
  examStatuses: any;

  constructor(private _router: Router, private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) {
    // get all exam statuses
    _context.model('CourseExamStatuses')
      .asQueryable()
      .select('identifier as id, alternateName as statusName')
      .getItems()
      .then(result => {
        this.examStatuses = result;
      });
  }

  ngOnInit() {

  }

  onSearchKeyDown(event: any) {
    if (event.keyCode === 13) {
      this.table.search((<HTMLInputElement>event.target).value);
      return false;
    }
  }

  onSelectBoxChange(event: any) {
    // get value
    const value = parseInt((<HTMLSelectElement>event.target).value, 10);
    if (isNaN(value)) {
      this._router.navigate(
        [],
        {
          relativeTo: this._activatedRoute,
          queryParams: { $filter: '' },
          queryParamsHandling: 'merge'
        }).then(() => {
          this.table.fetch();
        });
      return;
    }
    this._router.navigate(
      [],
      {
        relativeTo: this._activatedRoute,
        queryParams: { $filter: `status eq ${value}` },
        queryParamsHandling: 'merge'
      }).then(() => {
        this.table.fetch();
      });
  }

}
