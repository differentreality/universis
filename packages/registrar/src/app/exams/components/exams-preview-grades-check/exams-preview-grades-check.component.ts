import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {ConfigurationService, ErrorService, LoadingService} from '@universis/common';
import {ApplicationSettings} from '../../../registrar-shared/registrar-shared.module';

@Component({
  selector: 'app-exams-preview-grades-check',
  templateUrl: './exams-preview-grades-check.component.html',
  styleUrls: ['./exams-preview-grades-check.component.scss']
})
export class ExamsPreviewGradesCheckComponent implements OnInit {

  public courseexamId;
  public courseExamAction: any;
  public useDigitalSignature = true;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _loadingService: LoadingService,
              private errorService: ErrorService,
              private _config: ConfigurationService

  ) { }

  async ngOnInit() {
    try {
      this._loadingService.showLoading();

      // get digital signature settings from app config file
      this.useDigitalSignature = !! (<ApplicationSettings>this._config.settings.app).useDigitalSignature;

      this.courseexamId = this._activatedRoute.snapshot.parent.params.id;
      const gradeId = this._activatedRoute.snapshot.params.id;
      this.courseExamAction = await this._context.model(`CourseExams/${this.courseexamId}/actions`)
        .asQueryable()
        .expand('grades,owner,object($expand=examPeriod,course($expand=department($expand=currentPeriod,currentYear)),classes($expand=courseClass($expand=period,year)),examPeriod,year)')
        .where('id').equal(gradeId)
        .getItem();

      this._loadingService.hideLoading();
    } catch (err) {
      this._loadingService.hideLoading();
      return this.errorService.navigateToError(err);
    }

  }

}
