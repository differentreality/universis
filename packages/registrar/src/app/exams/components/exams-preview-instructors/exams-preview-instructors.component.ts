import { Component, OnInit, ViewChild } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AdvancedTableComponent, AdvancedTableConfiguration} from '../../../tables/components/advanced-table/advanced-table.component';
import {AngularDataContext} from '@themost/angular';
import * as EXAMS_INSTRUCTORS_LIST_CONFIG from './exams-instructors-table.config.json';

@Component({
  selector: 'app-exams-preview-instructors',
  templateUrl: './exams-preview-instructors.component.html',
  styleUrls: ['./exams-preview-instructors.component.scss']
})
export class ExamsPreviewInstructorsComponent implements OnInit {

  public readonly config = EXAMS_INSTRUCTORS_LIST_CONFIG;
  @ViewChild('instructors') instructors: AdvancedTableComponent;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext
  ) { }

  async ngOnInit() {
    this.instructors.query = this._context.model(`CourseExams/${this._activatedRoute.snapshot.params.id}/instructors`)
    .asQueryable().prepare();

    this.instructors.config = AdvancedTableConfiguration.cast(EXAMS_INSTRUCTORS_LIST_CONFIG);
  }

}
