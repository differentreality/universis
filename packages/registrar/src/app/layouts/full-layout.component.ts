import {Component, OnInit, ViewChild} from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import {AppSidebarService} from '../registrar-shared/services/app-sidebar.service';
import {ConfigurationService, UserService} from '@universis/common';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html',
  styles: [
      `
    button.dropdown-item {
      cursor: pointer;
    }
    `
  ]
})
export class FullLayoutComponent implements OnInit {

  constructor(private _context: AngularDataContext,
              private _configurationService: ConfigurationService,
              private _userService: UserService,
              private _activatedRoute: ActivatedRoute,
              private _appSidebarService: AppSidebarService) {
    //
  }

  @ViewChild('appSidebarNav') appSidebarNav: any;
  public status = { isOpen: false };
  public user;
  public today = new Date();
  public currentLang;
  public languages;

  ngOnInit(): void {
    // get sidebar navigation items
    this.appSidebarNav.navItems = this._appSidebarService.navigationItems;
    // get current user
    this._userService.getUser().then( user => {
      this.user = user;
    });
    // get current language
    this.currentLang = this._configurationService.currentLocale;
    // get languages
    this.languages = this._configurationService.settings.i18n.locales;
  }

  changeLanguage(lang) {
    this._configurationService.currentLocale = lang;
    // reload current route
    window.location.reload();
  }
}
