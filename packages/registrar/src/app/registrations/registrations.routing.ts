import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RegistrationsHomeComponent} from './components/registrations-home/registrations-home.component';
import {RegistrationsTableComponent} from './components/registrations-table/registrations-table.component';
import {RegistrationsPreviewComponent} from './components/registrations-preview/registrations-preview.component';
import {RegistrationsRootComponent} from './components/registrations-root/registrations-root.component';
import {RegistrationsPreviewGeneralComponent} from './components/registrations-preview/registrations-preview-general/registrations-preview-general.component';
import { RegistrationsPreviewLatestHistoryComponent } from './components/registrations-preview/registrations-preview-latest-history/registrations-preview-latest-history.component';

const routes: Routes = [
    {
        path: '',
        component: RegistrationsHomeComponent,
        data: {
            title: 'Registrations'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list'
            },
            {
                path: 'list',
                component: RegistrationsTableComponent,
                data: {
                    title: 'Registrations List'
                }
            },
            {
                path: 'active',
                component: RegistrationsTableComponent,
                data: {
                    title: 'Active SchoRegistrationslarships'
                }
            }
        ]
    },
    {
        path: ':id',
        component: RegistrationsRootComponent,
        data: {
            title: 'Registrations Home'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'preview'
            },
            {
                path: 'preview',
                component: RegistrationsPreviewComponent,
                data: {
                    title: 'Registrations Preview'
                },
                children: [
                    {
                        path: '',
                        redirectTo: 'general'
                    },
                    {
                        path: 'general',
                        component: RegistrationsPreviewGeneralComponent,
                        data: {
                            title: 'Registration\'s View'
                        }
                    },
                    {
                        path: 'history',
                        component: RegistrationsPreviewLatestHistoryComponent,
                        data: {
                            title: 'Registration\'s History'
                        }
                    }
                ]

            }
        ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class RegistrationsRoutingModule {
}
