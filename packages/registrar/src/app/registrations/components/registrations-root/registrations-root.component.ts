import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-registrations-root',
  template: '<router-outlet></router-outlet>',
})
export class RegistrationsRootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
