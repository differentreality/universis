import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@universis/common';
import { TranslateModule } from '@ngx-translate/core';
import { TablesModule } from '../tables/tables.module';
import { RegistrationsRoutingModule } from './registrations.routing';
import { RegistrationsSharedModule } from './registrations.shared';
import { RegistrationsTableComponent } from './components/registrations-table/registrations-table.component';
import { RegistrationsPreviewComponent } from './components/registrations-preview/registrations-preview.component';
import { RegistrationsPreviewGeneralComponent } from './components/registrations-preview/registrations-preview-general/registrations-preview-general.component';
import { RegistrationsHomeComponent } from './components/registrations-home/registrations-home.component';
import { RegistrationsRootComponent } from './components/registrations-root/registrations-root.component';
import { ElementsModule } from '../elements/elements.module';
import { StudentsSharedModule } from '../students/students.shared';
import { CoursesSharedModule } from '../courses/courses.shared';
import { NgPipesModule } from 'ngx-pipes';
import { RegistrationsPreviewLatestHistoryComponent } from './components/registrations-preview/registrations-preview-latest-history/registrations-preview-latest-history.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    TranslateModule,
    TablesModule,
    RegistrationsRoutingModule,
    RegistrationsSharedModule,
    ElementsModule,
    StudentsSharedModule,
    CoursesSharedModule,
    NgPipesModule
  ],
  declarations: [RegistrationsTableComponent, RegistrationsPreviewComponent, RegistrationsPreviewGeneralComponent, RegistrationsHomeComponent, RegistrationsRootComponent, RegistrationsPreviewLatestHistoryComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RegistrationsModule { }
