import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from '../../environments/environment';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@universis/common';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    TranslateModule,
  ],
  declarations: [
    
  ],
  exports: [
    
  ]
})
export class DepartmentsSharedModule implements OnInit {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading departments shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/departments.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }

}
