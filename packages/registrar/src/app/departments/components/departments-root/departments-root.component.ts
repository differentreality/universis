import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-departments-root',
  template: '<router-outlet></router-outlet>',
})
export class DepartmentsRootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
