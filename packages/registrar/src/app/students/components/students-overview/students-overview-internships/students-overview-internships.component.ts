import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-students-overview-internships',
  templateUrl: './students-overview-internships.component.html',
  styleUrls: ['./students-overview-internships.component.scss']
})
export class StudentsOverviewInternshipsComponent implements OnInit {

  public internships: any;
  @Input() studentId: number;

  constructor(private _context: AngularDataContext) { }

  async ngOnInit() {
    this.internships = await this._context.model('Internships')
      .asQueryable()
      .where('student').equal(this.studentId)
      .getItems();
  }
}
