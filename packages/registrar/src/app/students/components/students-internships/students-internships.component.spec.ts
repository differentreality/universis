import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsInternshipsComponent } from './students-internships.component';

describe('StudentsInternshipsComponent', () => {
  let component: StudentsInternshipsComponent;
  let fixture: ComponentFixture<StudentsInternshipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsInternshipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsInternshipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
