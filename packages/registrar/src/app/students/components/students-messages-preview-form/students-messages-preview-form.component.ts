import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import { RequestsService } from '../../../requests/services/requests.service';

@Component({
  selector: 'app-students-messages-preview-form',
  templateUrl: './students-messages-preview-form.component.html'
})

export class StudentsMessagesPreviewFormComponent implements OnInit {

  @Input() model: any;

  constructor(private _requestsService: RequestsService) {
  }

  async ngOnInit() {

  }

  // download the document file
  downloadFile(attachments) {
    this._requestsService.downloadFile(attachments);
  }
  
}
