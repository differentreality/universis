import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'app-students-courses',
  templateUrl: './students-courses.component.html',
  styleUrls: ['./students-courses.component.scss']
})

export class StudentsCoursesComponent implements OnInit {
  public studentId;
  public showMore = false;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext) {
  }

  ngOnInit() {
    this.studentId = this._activatedRoute.snapshot.params.id;
  }
}

