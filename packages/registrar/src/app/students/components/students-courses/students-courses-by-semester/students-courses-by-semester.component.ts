import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'app-students-courses-by-semester',
  templateUrl: './students-courses-by-semester.component.html',
  styleUrls: ['./students-courses-by-semester.component.scss']
})
export class StudentsCoursesBySemesterComponent implements OnInit {
  public model: any;
  public data: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext) {
  }

  async  ngOnInit() {

    this.model = await this._context.model('StudentCourses')
      .where('student').equal(this._activatedRoute.snapshot.params.id)
      .expand('course($expand=instructor),gradeExam($expand=instructors($expand=instructor))')
      .take(-1)
      .getItems();

    this.data = this.model;
    const studentCourses = this.data;
    this.data = this.data.filter(studentCourse => {
      return studentCourse.courseStructureType && studentCourse.courseStructureType.id !== 8;
    });
    // add courseParts as courses to each parent course
    this.data.forEach( studentCourse => {
      // get course parts
      if (studentCourse.courseStructureType && studentCourse.courseStructureType.id === 4) {
        studentCourse.courses = studentCourses.filter(course => {
          return course.parentCourse === studentCourse.course.id;
        }).sort((a, b) => {
          return a.course.displayCode < b.course.displayCode ? -1 : 1;
        });
      }
    });
  }
}
