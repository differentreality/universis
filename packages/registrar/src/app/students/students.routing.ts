import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StudentsHomeComponent} from './components/students-home/students-home.component';
import {StudentsTableComponent} from './components/students-table/students-table.component';
import {StudentsRootComponent} from './components/students-root/students-root.component';
import {StudentsGeneralComponent} from './components/students-general/students-general.component';
import {StudentsGradesComponent} from './components/students-grades/students-grades.component';
import {StudentsThesesComponent} from './components/students-theses/students-theses.component';
import {StudentsOverviewComponent} from './components/students-overview/students-overview.component';
import {StudentsCoursesComponent} from './components/students-courses/students-courses.component';
import {StudentsRegistrationsComponent} from './components/students-registrations/students-registrations.component';
import {StudentsScholarshipsComponent} from './components/students-scholarships/students-scholarships.component';
import {StudentsRequestsComponent} from './components/students-requests/students-requests.component';
import {StudentsInternshipsComponent} from './components/students-internships/students-internships.component';
import {StudentsGraduatedComponent} from './components/students-graduated/students-graduated.component';
import {StudentsMessagesComponent} from './components/students-messages/students-messages.component';

const routes: Routes = [
  {
    path: '',
    component: StudentsHomeComponent,
    data: {
      title: 'Students'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list'
      },
      {
        path: 'list',
        component: StudentsTableComponent,
        data: {
          title: 'Students List'
        }
      },
      {
        path: 'graduated',
        component: StudentsGraduatedComponent,
        data: {
          title: 'Students Graduated'
        }
      }
    ]
  },
  {
    path: ':id',
    component: StudentsRootComponent,
    data: {
      title: 'Student Home'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'overview'
      },
      {
        path: 'overview',
        component: StudentsOverviewComponent,
        data: {
          title: 'Students.Overview'
        }
      },
      {
        path: 'general',
        component: StudentsGeneralComponent,
        data: {
          title: 'Students.General'
        }
      },
      {
        path: 'courses',
        component: StudentsCoursesComponent,
        data: {
          title: 'Students.Courses'
        }
      },
      {
        path: 'theses',
        component: StudentsThesesComponent,
        data: {
          title: 'Students.Theses'
        }
      },
      {
        path: 'registrations',
        component: StudentsRegistrationsComponent,
        data: {
          title: 'Students.Registrations'
        }
      },
      {
        path: 'grades',
        component: StudentsGradesComponent,
        data: {
          title: 'Students.Grades'
        }
      },
      {
        path: 'requests',
        component: StudentsRequestsComponent,
        data: {
          title: 'Students.Requests'
        }
      },
      {
        path: 'scholarships',
        component: StudentsScholarshipsComponent,
        data: {
          title: 'Students.Scholarships'
        }
      },
      {
        path: 'internships',
        component: StudentsInternshipsComponent,
        data: {
          title: 'Students.Internships'
        }
      },
      {
        path: 'messages',
        component: StudentsMessagesComponent,
        data: {
          title: 'Students.Messages'
        }
      }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class StudentsRoutingModule {
}
