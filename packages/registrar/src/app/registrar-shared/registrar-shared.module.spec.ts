import {TestBed, async, inject} from '@angular/core/testing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {RegistrarSharedModule} from './registrar-shared.module';
describe('InstructorsModule', () => {
  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot()
      ]
    }).compileComponents();
  }));
  it('should create an instance', inject([TranslateService], (translateService: TranslateService) => {
    const registrarSharedModule = new RegistrarSharedModule(null, translateService);
    expect(registrarSharedModule).toBeTruthy();
  }));
});
