import { Component, Input, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-study-programs-preview-general',
  templateUrl: './study-programs-preview-general.component.html'
})
export class StudyProgramsPreviewGeneralComponent implements OnInit {
  public model: any;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) {
}

  async ngOnInit() {
    this.model = await this._context.model('StudyPrograms')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('department,studyLevel')
      .getItem();
  }

}
