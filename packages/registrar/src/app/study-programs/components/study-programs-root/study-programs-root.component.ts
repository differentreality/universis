import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-study-programs-root',
  template: '<router-outlet></router-outlet>',
})
export class StudyProgramsRootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
