import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyProgramsHomeComponent } from './study-programs-home.component';

describe('StudyProgramsHomeComponent', () => {
  let component: StudyProgramsHomeComponent;
  let fixture: ComponentFixture<StudyProgramsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyProgramsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyProgramsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
