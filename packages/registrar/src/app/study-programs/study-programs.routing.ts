import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StudyProgramsHomeComponent} from './components/study-programs-home/study-programs-home.component';
import {StudyProgramsTableComponent} from './components/study-programs-table/study-programs-table.component';
import {StudyProgramsRootComponent} from './components/study-programs-root/study-programs-root.component';
import {StudyProgramsPreviewComponent} from './components/study-programs-preview/study-programs-preview.component';
import {StudyProgramsPreviewGeneralComponent} from './components/study-programs-preview/study-programs-preview-general/study-programs-preview-general.component';
import {StudyProgramsPreviewCoursesComponent} from './components/study-programs-preview/study-programs-preview-courses/study-programs-preview-courses.component';
import { ProgramCoursePreviewComponent } from './components/program-course-preview/program-course-preview.component';
import { ProgramCoursePreviewGeneralComponent } from './components/program-course-preview/program-course-preview-general/program-course-preview-general.component';

const routes: Routes = [
    {
        path: '',
        component: StudyProgramsHomeComponent,
        data: {
            title: 'Study Programs'
        },
        children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: 'list'
        },
        {
          path: 'list',
          component: StudyProgramsTableComponent,
          data: {
            title: 'Study Programs List'
          }
        },
        {
          path: 'active',
          component: StudyProgramsTableComponent,
          data: {
            title: 'Active Study Programs'
          }
        }
      ]
    },
    {
     path: ':id',
        component: StudyProgramsRootComponent,
        data: {
            title: 'Study Programs Home'
        },
        children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: 'preview'
        },
        {
          path: 'preview',
          component: StudyProgramsPreviewComponent,
          data: {
            title: 'Study Programs Preview'
          },
          children: [
            {
                path: '',
                redirectTo: 'general'
            },
            {
                path: 'general',
                component: StudyProgramsPreviewGeneralComponent,
                data: {
                    title: 'Study Programs Preview General'
                }
            },
            {
                path: 'courses',
                component: StudyProgramsPreviewCoursesComponent,
                data: {
                    title: 'Study Programs Preview Courses'
                }
            }
        ]

        }
      ]
    },
    {
      path: ':ProgramID/courses/:CourseID',
      component: ProgramCoursePreviewComponent,
      data: {
        title: 'Study Programs Preview'
      },
      children: [
        {
            path: '',
            redirectTo: 'general'
        },
        {
            path: 'general',
            component: ProgramCoursePreviewGeneralComponent,
            data: {
                title: 'Study Programs Preview General'
            }
        }
      ]

    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class StudyProgramsRoutingModule {
}
