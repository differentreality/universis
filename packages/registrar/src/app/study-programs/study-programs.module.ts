import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudyProgramsHomeComponent } from './components/study-programs-home/study-programs-home.component';
import {StudyProgramsRoutingModule} from './study-programs.routing';
import {StudyProgramsSharedModule} from './study-programs.shared';
import { StudyProgramsTableComponent } from './components/study-programs-table/study-programs-table.component';
import {TranslateModule} from '@ngx-translate/core';
import {TablesModule} from '../tables/tables.module';
import { StudyProgramsRootComponent } from './components/study-programs-root/study-programs-root.component';
import { StudyProgramsPreviewComponent } from './components/study-programs-preview/study-programs-preview.component';
import { StudyProgramsPreviewGeneralComponent } from './components/study-programs-preview/study-programs-preview-general/study-programs-preview-general.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '@universis/common';
import { StudyProgramsPreviewCoursesComponent } from './components/study-programs-preview/study-programs-preview-courses/study-programs-preview-courses.component';
import { CoursesSharedModule } from '../courses/courses.shared';
import {ElementsModule} from '../elements/elements.module';
import { ProgramCoursePreviewComponent } from './components/program-course-preview/program-course-preview.component';
import { ProgramCoursePreviewGeneralComponent } from './components/program-course-preview/program-course-preview-general/program-course-preview-general.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    TablesModule,
    StudyProgramsSharedModule,
    StudyProgramsRoutingModule,
    SharedModule,
    FormsModule,
    CoursesSharedModule,
    ElementsModule
  ],
  declarations: [StudyProgramsHomeComponent, StudyProgramsTableComponent, StudyProgramsRootComponent, StudyProgramsPreviewComponent, StudyProgramsPreviewGeneralComponent, StudyProgramsPreviewCoursesComponent, ProgramCoursePreviewComponent, ProgramCoursePreviewGeneralComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StudyProgramsModule { }
