import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesPreviewFormComponent } from './classes-preview-form.component';

describe('ClassesPreviewFormComponent', () => {
  let component: ClassesPreviewFormComponent;
  let fixture: ComponentFixture<ClassesPreviewFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassesPreviewFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesPreviewFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
