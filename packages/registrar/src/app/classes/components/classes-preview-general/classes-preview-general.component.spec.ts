import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesPreviewGeneralComponent } from './classes-preview-general.component';

describe('ClassesPreviewGeneralComponent', () => {
  let component: ClassesPreviewGeneralComponent;
  let fixture: ComponentFixture<ClassesPreviewGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassesPreviewGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesPreviewGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
