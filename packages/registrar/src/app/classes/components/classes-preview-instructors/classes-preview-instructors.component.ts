import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AdvancedTableComponent, AdvancedTableConfiguration} from '../../../tables/components/advanced-table/advanced-table.component';
import {AngularDataContext} from '@themost/angular';
import * as CLASSES_INSTRUCTORS_LIST_CONFIG from './classes-preview-instructors.config.json';

@Component({
  selector: 'app-classes-preview-instructors',
  templateUrl: './classes-preview-instructors.component.html'
})
export class ClassesPreviewInstructorsComponent implements OnInit {

  public readonly config = CLASSES_INSTRUCTORS_LIST_CONFIG;
  @ViewChild('instructors') instructors: AdvancedTableComponent;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {

    this.instructors.query =  this._context.model('CourseClasses/' + this._activatedRoute.snapshot.params.id + '/instructors')
      .asQueryable()
      .expand('instructor')
      .prepare();

    this.instructors.config = AdvancedTableConfiguration.cast(CLASSES_INSTRUCTORS_LIST_CONFIG);
  }

}
