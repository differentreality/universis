import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CoursesHomeComponent} from './components/courses-home/courses-home.component';
import {CoursesTableComponent} from './components/courses-table/courses-table.component';
import {CoursesRootComponent} from './components/courses-root/courses-root.component';
import {CoursesPreviewGeneralComponent} from './components/courses-preview-general/courses-preview-general.component';
import {CoursesPreviewClassesComponent} from './components/courses-preview-classes/courses-preview-classes.component';
import {CoursesOverviewComponent} from './components/courses-overview/courses-overview.component';
import {CoursesExamsComponent} from './components/courses-exams/courses-exams.component';
import {CoursesStudyProgramsComponent} from './components/courses-study-programs/courses-study-programs.component';

const routes: Routes = [
    {
        path: '',
        component: CoursesHomeComponent,
        data: {
            title: 'Courses'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'list'
          },
          {
            path: 'list',
            component: CoursesTableComponent,
            data: {
              title: 'Courses List'
            }
          },
          {
            path: 'active',
            component: CoursesTableComponent,
            data: {
              title: 'Active Courses'
            }
          }
        ]
    },
    {
      path: ':id',
      component: CoursesRootComponent,
      data: {
        title: 'Courses Root'
      },
          children: [
            {
              path: '',
              redirectTo: 'overview'
            },
            {
              path: 'overview',
              component: CoursesOverviewComponent,
              data: {
                title: 'Courses.Overview'
              }
            },
            {
              path: 'general',
              component: CoursesPreviewGeneralComponent,
              data: {
                title: 'Courses.General'
              }
            },
            {
              path: 'classes',
              component: CoursesPreviewClassesComponent,
              data: {
                title: 'Courses.Classes'
              }
            },
            {
              path: 'exams',
              component: CoursesExamsComponent,
              data: {
                title: 'Courses.Exams'
              }
            },
            {
              path: 'study-programs',
              component: CoursesStudyProgramsComponent,
              data: {
                title: 'Courses.StudyPrograms'
              }
            }
          ]
        }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class CoursesRoutingModule {
}
