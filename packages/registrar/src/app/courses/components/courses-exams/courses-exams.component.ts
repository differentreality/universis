import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-courses-students',
  templateUrl: './courses-exams.component.html',
  styleUrls: ['./courses-exams.component.scss']
})
export class CoursesExamsComponent implements OnInit {
  public model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {

    this.model = await this._context.model('CourseExams')
      .where('course').equal(this._activatedRoute.snapshot.params.id)
      .expand('examPeriod,status,completedByUser,year,course($expand=department)')
      .orderByDescending('year')
      .thenByDescending('examPeriod')
      .getItems();
  }

}
