import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-courses-root',
  templateUrl: './courses-root.component.html'
})
export class CoursesRootComponent implements OnInit {
  public model: any;
  public tabs: any[];

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.tabs = this._activatedRoute.routeConfig.children.filter( route => typeof route.redirectTo === 'undefined' );

    this.model = await this._context.model('Courses')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .select('id', 'name','displayCode')
      .getItem();
  }

}
