import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-courses-overview-exams',
  templateUrl: './courses-overview-exams.component.html',
  styleUrls: ['./courses-overview-exams.component.scss']
})
export class CoursesOverviewExamsComponent implements OnInit {
  public model: any;
  public grading: any;
  @Input() currentYear: any;
  public courseId: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) { }

  async ngOnInit() {
    this.model = await this._context.model('CourseExams')
      .where('course').equal(this._activatedRoute.snapshot.params.id)
      .expand('examPeriod,status,completedByUser,year,course($expand=department)')
      .and('year').equal({'$name': '$it/course/department/currentYear'})
      .getItems();

    this.grading = await this._context.model('CourseExams/' + this._activatedRoute.snapshot.params.id + '/actions')
      .asQueryable()
      .expand('modifiedBy,additionalResult($expand=user,courseExam($expand=completedByUser,examPeriod,status))')
      .where('additionalResult').notEqual(null)
      .orderByDescending('additionalResult/dateCreated')
      .take(-1)
      .getItems();

    this.courseId = this._activatedRoute.snapshot.params.id;
  }

}
