import {Component, OnInit, ViewChild} from '@angular/core';
import * as CLASSES_LIST_CONFIG from '../../../courses/components/courses-preview-classes/courses-preview-classes.config.json';
import {AdvancedTableComponent, AdvancedTableConfiguration} from '../../../tables/components/advanced-table/advanced-table.component';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-courses-preview-classes',
  templateUrl: './courses-preview-classes.component.html',
})
export class CoursesPreviewClassesComponent implements OnInit {
  public readonly config = CLASSES_LIST_CONFIG;
  @ViewChild('classes') classes: AdvancedTableComponent;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.classes.query =  this._context.model('courseClasses')
      .where('course').equal(this._activatedRoute.snapshot.params.id)
      .orderByDescending('year')
      .thenBy('period')
      .prepare();

    this.classes.config = AdvancedTableConfiguration.cast(CLASSES_LIST_CONFIG);
  }
}
