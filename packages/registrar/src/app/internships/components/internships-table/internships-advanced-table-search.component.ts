import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AdvancedTableSearchComponent } from '../../../tables/components/advanced-table/advanced-table-search.component';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-internships-advanced-table-search',
    templateUrl: './internships-advanced-table-search.component.html',
    encapsulation: ViewEncapsulation.None
})

export class InternshipsAdvancedTableSearchComponent {

    @Input() filter: any;

    constructor(_context: AngularDataContext, _activatedRoute: ActivatedRoute, datePipe: DatePipe) {
        //
    }
}
