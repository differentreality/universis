import {Component, Input, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import { AdvancedTableComponent } from '../../../tables/components/advanced-table/advanced-table.component';
import { ActivatedRoute } from '@angular/router';
import { template, at } from 'lodash';
import { AngularDataContext } from '@themost/angular';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-advanced-table-search',
    templateUrl: './advanced-table-search.component.html',
    encapsulation: ViewEncapsulation.None
})

export class AdvancedTableSearchComponent implements OnInit {

    @Input() table: AdvancedTableComponent;
    @Input() collapsed = true;

    @Input() filter: any = {};

    constructor(protected _context: AngularDataContext, protected _activatedRoute: ActivatedRoute, protected datePipe: DatePipe) {

    }
    ngOnInit(): void {

    }

    onSearchKeyDown(event: any) {
        if (this.table && event.keyCode === 13) {
            this.table.search((<HTMLInputElement>event.target).value);
            return false;
        }
    }

    transformDate(date) {
        date = this.datePipe.transform(date, 'MM/dd/yyyy'); // whatever format you need. 
        return date;
    }

    convertToString(nameFilter) {
        let name = '';
        if (nameFilter != null) {
            name = nameFilter.toString();
        }
        return name;
    }

    apply() {
        if (Array.isArray(this.table.config.criteria)) {
            const expressions = [];
            this.table.config.criteria.forEach(x => {
                if (this.filter[x.name]) {
                    const nameFilter = this.convertToString(this.filter[x.name]);
                    if (nameFilter.includes('GMT')) {
                        const newDate = this.transformDate(this.filter[x.name]);
                        expressions.push(template(x.filter)({
                            value: newDate
                        }));
                    } else if (this.filter[x.name] !== 'undefined') {
                        expressions.push(template(x.filter)({
                            value: this.filter[x.name]
                        }));
                    }
                }
            });
            if (expressions || expressions.length) {
                // create client query
                const query = this._context.model(this.table.config.model).asQueryable();
                query.setParam('filter', expressions.join(' and ')).prepare();
                this.table.query = query;
                this.table.fetch();
            }
        }
        return false;
    }

    onChange(event: any) {
        if (Array.isArray(this.table.config.criteria)) {
            const expressions = [];
            this.table.config.criteria.forEach(x => {
                if (this.filter[x.name]) {
                    const nameFilter = this.convertToString(this.filter[x.name]);
                    if (nameFilter.includes('GMT')) {
                        const newDate = this.transformDate(this.filter[x.name]);
                        expressions.push(template(x.filter)({
                            value: newDate
                        }));
                    } else if (this.filter[x.name] !== 'undefined') {
                        expressions.push(template(x.filter)({
                            value: this.filter[x.name]
                        }));
                    }
                }
            });
            if (expressions || expressions.length) {
                // create client query
                const query = this._context.model(this.table.config.model).asQueryable();
                query.setParam('filter', expressions.join(' and ')).prepare();
                this.table.query = query;
                this.table.fetch();
            }
        }
        return false;
    }
}
